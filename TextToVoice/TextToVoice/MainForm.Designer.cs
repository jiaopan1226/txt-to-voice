﻿namespace txt_to_voice
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btn_save_path = new System.Windows.Forms.Button();
            this.num_speed = new System.Windows.Forms.NumericUpDown();
            this.num_voice = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.num_pit = new System.Windows.Forms.NumericUpDown();
            this.cm_per = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.convert_progress_all = new System.Windows.Forms.ProgressBar();
            this.progress_label = new System.Windows.Forms.Label();
            this.btn_save_settings = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_cancelall = new System.Windows.Forms.Button();
            this.btn_checkedall = new System.Windows.Forms.Button();
            this.txt_reg_symbol = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_regStr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.preview = new System.Windows.Forms.Button();
            this.thread_quantity = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.num_section_start = new System.Windows.Forms.NumericUpDown();
            this.num_section_end = new System.Windows.Forms.NumericUpDown();
            this.btn_section = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.num_speed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_voice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_pit)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.thread_quantity)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_section_start)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_section_end)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_save_path
            // 
            this.btn_save_path.Location = new System.Drawing.Point(187, 284);
            this.btn_save_path.Name = "btn_save_path";
            this.btn_save_path.Size = new System.Drawing.Size(143, 23);
            this.btn_save_path.TabIndex = 0;
            this.btn_save_path.Text = "2、选择音频文件保存位置";
            this.btn_save_path.UseVisualStyleBackColor = true;
            this.btn_save_path.Click += new System.EventHandler(this.button1_Click);
            // 
            // num_speed
            // 
            this.num_speed.Location = new System.Drawing.Point(27, 26);
            this.num_speed.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.num_speed.Name = "num_speed";
            this.num_speed.Size = new System.Drawing.Size(120, 21);
            this.num_speed.TabIndex = 1;
            // 
            // num_voice
            // 
            this.num_voice.Location = new System.Drawing.Point(27, 70);
            this.num_voice.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num_voice.Name = "num_voice";
            this.num_voice.Size = new System.Drawing.Size(120, 21);
            this.num_voice.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(185, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "语速(语速，取值0-9，默认为5中语速)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "音量(音量，取值0-15，默认为5中音量)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(185, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "发音类别(发音人选择)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(185, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "音调(音调，取值0-9，默认为5中语调)";
            // 
            // num_pit
            // 
            this.num_pit.Location = new System.Drawing.Point(27, 114);
            this.num_pit.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.num_pit.Name = "num_pit";
            this.num_pit.Size = new System.Drawing.Size(120, 21);
            this.num_pit.TabIndex = 8;
            // 
            // cm_per
            // 
            this.cm_per.FormattingEnabled = true;
            this.cm_per.Location = new System.Drawing.Point(27, 158);
            this.cm_per.Name = "cm_per";
            this.cm_per.Size = new System.Drawing.Size(121, 20);
            this.cm_per.TabIndex = 9;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 353);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(152, 51);
            this.button2.TabIndex = 10;
            this.button2.Text = "转换";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 284);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "1、选择文本文件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // convert_progress_all
            // 
            this.convert_progress_all.Location = new System.Drawing.Point(177, 353);
            this.convert_progress_all.Name = "convert_progress_all";
            this.convert_progress_all.Size = new System.Drawing.Size(445, 25);
            this.convert_progress_all.TabIndex = 12;
            // 
            // progress_label
            // 
            this.progress_label.AutoSize = true;
            this.progress_label.Location = new System.Drawing.Point(185, 392);
            this.progress_label.Name = "progress_label";
            this.progress_label.Size = new System.Drawing.Size(53, 12);
            this.progress_label.TabIndex = 14;
            this.progress_label.Text = "整体进度";
            // 
            // btn_save_settings
            // 
            this.btn_save_settings.Location = new System.Drawing.Point(468, 154);
            this.btn_save_settings.Name = "btn_save_settings";
            this.btn_save_settings.Size = new System.Drawing.Size(94, 27);
            this.btn_save_settings.TabIndex = 16;
            this.btn_save_settings.Text = "保存配置";
            this.btn_save_settings.UseVisualStyleBackColor = true;
            this.btn_save_settings.Click += new System.EventHandler(this.btn_save_settings_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Location = new System.Drawing.Point(12, 436);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(610, 191);
            this.panel1.TabIndex = 19;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_section);
            this.groupBox1.Controls.Add(this.num_section_end);
            this.groupBox1.Controls.Add(this.num_section_start);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btn_cancelall);
            this.groupBox1.Controls.Add(this.btn_checkedall);
            this.groupBox1.Controls.Add(this.txt_reg_symbol);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txt_regStr);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Location = new System.Drawing.Point(661, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(518, 601);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "文本拆分";
            // 
            // btn_cancelall
            // 
            this.btn_cancelall.Location = new System.Drawing.Point(101, 569);
            this.btn_cancelall.Name = "btn_cancelall";
            this.btn_cancelall.Size = new System.Drawing.Size(75, 23);
            this.btn_cancelall.TabIndex = 8;
            this.btn_cancelall.Text = "全不选";
            this.btn_cancelall.UseVisualStyleBackColor = true;
            this.btn_cancelall.Click += new System.EventHandler(this.btn_cancelall_Click);
            // 
            // btn_checkedall
            // 
            this.btn_checkedall.Location = new System.Drawing.Point(17, 570);
            this.btn_checkedall.Name = "btn_checkedall";
            this.btn_checkedall.Size = new System.Drawing.Size(75, 23);
            this.btn_checkedall.TabIndex = 7;
            this.btn_checkedall.Text = "全选";
            this.btn_checkedall.UseVisualStyleBackColor = true;
            this.btn_checkedall.Click += new System.EventHandler(this.btn_checkedall_Click);
            // 
            // txt_reg_symbol
            // 
            this.txt_reg_symbol.Location = new System.Drawing.Point(101, 66);
            this.txt_reg_symbol.Name = "txt_reg_symbol";
            this.txt_reg_symbol.Size = new System.Drawing.Size(388, 21);
            this.txt_reg_symbol.TabIndex = 6;
            this.txt_reg_symbol.Text = "[.。]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "判断符号";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "判断章节标题";
            // 
            // txt_regStr
            // 
            this.txt_regStr.Location = new System.Drawing.Point(101, 39);
            this.txt_regStr.Name = "txt_regStr";
            this.txt_regStr.Size = new System.Drawing.Size(388, 21);
            this.txt_regStr.TabIndex = 3;
            this.txt_regStr.Text = "第[一二三四五六七八九十零百千0-9]+章";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "正则表达式：";
            // 
            // listView1
            // 
            this.listView1.CheckBoxes = true;
            this.listView1.Location = new System.Drawing.Point(17, 88);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(483, 475);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // preview
            // 
            this.preview.Location = new System.Drawing.Point(355, 284);
            this.preview.Name = "preview";
            this.preview.Size = new System.Drawing.Size(113, 23);
            this.preview.TabIndex = 1;
            this.preview.Text = "3、预览";
            this.preview.UseVisualStyleBackColor = true;
            this.preview.Click += new System.EventHandler(this.preview_Click);
            // 
            // thread_quantity
            // 
            this.thread_quantity.Location = new System.Drawing.Point(28, 203);
            this.thread_quantity.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.thread_quantity.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.thread_quantity.Name = "thread_quantity";
            this.thread_quantity.Size = new System.Drawing.Size(120, 21);
            this.thread_quantity.TabIndex = 21;
            this.thread_quantity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(185, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 12);
            this.label8.TabIndex = 22;
            this.label8.Text = "同时请求线程数";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1211, 25);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.关于ToolStripMenuItem.Text = "关于";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(262, 574);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 9;
            this.label9.Text = "起始章节：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(384, 574);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 10;
            this.label10.Text = "起始章节：";
            // 
            // num_section_start
            // 
            this.num_section_start.Location = new System.Drawing.Point(328, 570);
            this.num_section_start.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.num_section_start.Name = "num_section_start";
            this.num_section_start.Size = new System.Drawing.Size(50, 21);
            this.num_section_start.TabIndex = 24;
            // 
            // num_section_end
            // 
            this.num_section_end.Location = new System.Drawing.Point(455, 569);
            this.num_section_end.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.num_section_end.Name = "num_section_end";
            this.num_section_end.Size = new System.Drawing.Size(50, 21);
            this.num_section_end.TabIndex = 25;
            // 
            // btn_section
            // 
            this.btn_section.Location = new System.Drawing.Point(182, 570);
            this.btn_section.Name = "btn_section";
            this.btn_section.Size = new System.Drawing.Size(75, 23);
            this.btn_section.TabIndex = 26;
            this.btn_section.Text = "区间选择";
            this.btn_section.UseVisualStyleBackColor = true;
            this.btn_section.Click += new System.EventHandler(this.btn_section_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1211, 647);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.thread_quantity);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_save_settings);
            this.Controls.Add(this.preview);
            this.Controls.Add(this.progress_label);
            this.Controls.Add(this.convert_progress_all);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cm_per);
            this.Controls.Add(this.num_pit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.num_voice);
            this.Controls.Add(this.num_speed);
            this.Controls.Add(this.btn_save_path);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "txt-to-voice";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.num_speed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_voice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_pit)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.thread_quantity)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_section_start)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_section_end)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_save_path;
        private System.Windows.Forms.NumericUpDown num_speed;
        private System.Windows.Forms.NumericUpDown num_voice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown num_pit;
        private System.Windows.Forms.ComboBox cm_per;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar convert_progress_all;
        private System.Windows.Forms.Label progress_label;
        private System.Windows.Forms.Button btn_save_settings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_regStr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button preview;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TextBox txt_reg_symbol;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown thread_quantity;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_cancelall;
        private System.Windows.Forms.Button btn_checkedall;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.Button btn_section;
        private System.Windows.Forms.NumericUpDown num_section_end;
        private System.Windows.Forms.NumericUpDown num_section_start;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}

