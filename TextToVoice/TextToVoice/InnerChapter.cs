﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextToVoice
{
  public  class InnerChapter
    {
        public InnerChapter()
        {
            this.content = new StringBuilder();
        }
        public int no { get; set; }
        public string title { get; set; }
        public StringBuilder content { get; set; }
    }
}
